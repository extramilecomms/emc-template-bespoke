# EMC WordPress Template with Parcel Tailwind

## Install node packages

1. In Terminal navigate to wp-content/themes/emc folder
2. Run `npm i` in the terminal to install the node packages

## Install and configure the database

1. Import the starter database from the wp-content/themes/emc/bk folder
2. Update the site_url and home fields in the wp_options table
3. Add the database credentials to the wp-config.php file

## Parcel

There are two npm commands configured in package.json:

1. `npm run watch` which is used for development and includes browser hot reloading
2. `npm run prod` which is used to compile the JS and CSS assets before the site is launched - not used for development

## Tailwind

Tailwind has been incorporated into the theme already.

Site specific customisations can be made using the tailwind.config.js.

### New utility classes

These can be added in the assets/styles/partials/_tailwind.less file.  See docs for more details: https://tailwindcss.com/docs/adding-new-utilities

### Extracting Components

Utility classes can be combined into a single class to prevent duplication. Very useful for common components such as buttons.
See Extracting component classes with @apply for more details: https://tailwindcss.com/docs/extracting-components
Extracted component classes should be added to the assets/styles/partials/_tailwind.less file.

## Template blocks

These are found under markup/sections and the two-column.php is an example block which has been modified to work with Tailwind using our preferred syntax / structure.