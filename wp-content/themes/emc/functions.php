<?php

/**
 * Dump the given content and then die.
 *
 * @var mixed $content
 */
function dd($content, $die = false) {
    // Print content.
    echo '<pre style="width: 100%; max-width: 100%; background-color: #fafafa; color: #4a4a4a; font-size: 16px; line-height: 1.75; letter-spacing: -0.5px; white-space: pre-wrap; padding: 20px; margin: 30px auto; border: 2px solid #eaeaea; border-radius: 5px; box-sizing: border-box;">';
    print_r($content);
    echo '</pre>';

    // Die only if told to.
    if($die) {
        die;
    }
}




/**
 * Register website menus for use with WordPress.
 */
function register_menu() {
	register_nav_menus([
		'main-menu'   => 'Main Menu',
		'footer-menu' => 'Footer Menu'
	]);
}
add_action('after_setup_theme', 'register_menu');


/**
 * Add Website Options ACF Pro tab to dashboard.
 */
if(function_exists('acf_add_options_page')) {
	acf_add_options_page([
		'page_title' => 'Website Options',
		'menu_title' => 'Website Options',
		'menu_slug'  => 'website-options'
	]);
}




/**
 * Add featured images to blog posts.
 */
add_theme_support('post-thumbnails');


/**
 * Redirect from /?s=value to /search/value.
 */
function pretty_search() {
    if (is_search() && !empty($_GET['s'])) {
        wp_redirect(home_url('/search/').urlencode(get_query_var('s')));
        exit();
    }
}
add_action('template_redirect', 'pretty_search');


/**
 * Return the post excerpt cut down to the given length.
 *
 * @param integer $length
 *
 * @return string
 */
function custom_excerpt($length = 20) {
	return implode(' ', array_slice(explode(' ', get_the_excerpt()), 0, $length)) . '...';
}


/**
 * Restructure the WordPress admin menu.
 *
 * @link https://developer.wordpress.org/reference/functions/add_menu_page/#menu-structure
 * @see ./custom-post-types.php
 */
function fb_structure_admin_menu() {
    // Load the global menu arrays.
    global $menu;
    global $submenu;

    // Unset all of the default WordPress menu items which aren't required.
    unset($menu[15]); // Links.
    unset($menu[25]); // Comments.
    unset($menu[75]); // Tools.
    unset($submenu['themes.php'][5]); // Appearance -> Themes.
    unset($submenu['themes.php'][6]); // Appearance -> Customise.
}
add_action('admin_menu', 'fb_structure_admin_menu');



/**
 * Enqueue all style/script assets.
 *
 * @return void
 */
function enqueue_assets() {
    // Handle, source, dependencies, URL parameters.
    wp_enqueue_style('style-css', get_stylesheet_directory_uri() . '/dist/styles/style.css', [], null);
    // Handle, source, dependencies, URL parameters, enqueue to footer.
    wp_enqueue_script('script-js', get_stylesheet_directory_uri() . '/dist/js/script.js', ['jquery'], null, true);
}
add_action('wp_enqueue_scripts', 'enqueue_assets');


/**
 * Remove the unnecessary default WordPress assets.
 *
 * @return void
 */
function remove_default_assets() {
    // Script used to embed other WordPress blog posts on this website.
    wp_deregister_script('wp-embed');
    // Gutenberg block styling.
    wp_dequeue_style('wp-block-library');
}
add_action('wp_enqueue_scripts', 'remove_default_assets');

// Emoji libraries.
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');


/**
 * Custom functions.
 */

/**
 * Add a search icon to the end of the main navigation bar.
 *
 * @param string $items
 * @param stdClass $args
 *
 * @return string
 */
function add_search_icon($items, $args) {
    if($args->theme_location == 'main-menu' && $args->items_wrap != '%3$s') {
        $items .= '<li class="menu-item search-button-container"><a href="#" class="toggle-search" aria-label="Toggle the search bar"><i class="fas fa-search"></i></a></li>';
    }

    return $items;
}
add_filter('wp_nav_menu_items', 'add_search_icon', 10, 2);

?>
