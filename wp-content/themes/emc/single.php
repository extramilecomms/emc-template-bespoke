<?php

get_header();

global $post;


// Banner.
$data = [
    'size'      => 'medium',
    'animation' => 'default',
    'bcrumbs'   => 'no',
    'slides'    => [
        [
            'pb_b_slide_background' => get_the_post_thumbnail_url(),
            'pb_b_slide_headline'   => '',
            'pb_b_slide_content'    => '<h1>' . get_the_title() . '</h1><p>' . get_the_date() . ' / ' . get_the_category()[0]->name . '</p>',
            'pb_b_slide_buttons'    => [],
        ]
    ]
];
include __DIR__ . '/markup/sections/banner.php';

?>

<section class="page-content">
    <div class="wrapper">
        <div class="content">
            <div class="written">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<?php

get_footer();

?>
