// Third party.
require('./partials/modernizr.js');
var $ = window.jQuery; // = require('jquery');
require('tiny-slider');
// require('@fortawesome/fontawesome-free/js/all.js');
var CountUp = require('countup.js');

// Default functionality for navigation/search etc.
var navigation = require('./sections/navigation.js')($, Modernizr);
var search = require('./sections/search.js')($);

// Default functionality for each section required on the website.
var banner = require('./sections/banner.js')($);
var twocolumn = require('./sections/two-column.js')($, Modernizr);
var countup = require('./sections/count-up.js')($, CountUp, Modernizr);
var logocarousel = require('./sections/logo-carousel.js')($);
var stickyelements = require('./sections/sticky-elements.js')($);
var accordion = require('./sections/accordion.js')($);


$(document).ready(function() {

    /**
     * Custom functionality and overrides.
     */



});
