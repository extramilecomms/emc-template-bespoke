/**
* Call doScrollVisible() on page load and on page scroll.
*
* @param {string} selector
*
* @return {void}
*/
function scrollVisible(selector, offset) {
    // Elements selected with selector.
    var els = document.querySelectorAll(selector);
    // Height of the viewport window.
    var winheight = window.innerHeight;

    doScrollVisible(els, winheight, offset);

    // On scroll, loop through each element.
    document.addEventListener('scroll', function() {
        doScrollVisible(els, winheight, offset);

    }, Modernizr.passiveeventlisteners ? {passive: true} : false);
}

/**
 * Add a 'visible' class to all elements matching the given selector if they are visible in the viewport.
 *
 * @param {NodeList} els
 * @param {float}    winheight
 * @param {integer}  offset
 *
 * @return {void}
 */
function doScrollVisible(els, winheight, offset) {
    for (var i=0; i<els.length; i++) {
        // This element position relative to the viewport.
        var rect = els[i].getBoundingClientRect();
        // Element offset - how far from the top of the viewport is it?
        var eloffset = rect.top;
        // Height of the rendered element.
        var elheight = els[i].offsetHeight;

        // Add offsets to top/bottom of each element for the on-screen detection.
        // On screen X pixels into the element, and off screen X pixels before the end of the element.
        // The height needs to have double the offset, to take into consideration the offset added to the
        // top of the element.
        eloffset += offset;
        elheight -= (offset * 2);

        // If the top of the element has passed the bottom of the screen,
        // and the bottom of the element hasn't gone off the top of the screen.
        if ((eloffset <= winheight) && ((eloffset + elheight) >= 0)) {
            els[i].classList.add('visible');
        }
    }
}

module.exports = function($) {
    $(document).ready(function() {
        // Add visible classes to the given elements as they enter the viewport window.
        scrollVisible('.js.two-column', 200);
    });
}
