/**
* Call doScrollVisible() on page load and on page scroll.
*
* @param {CountUp} CountUp
* @param {string} selector
*
* @return {void}
*/
function scrollVisible(CountUp, selector, offset) {
    // Elements selected with selector.
    var els = document.querySelectorAll(selector);
    // Height of the viewport window.
    var winheight = window.innerHeight;

    doScrollVisible(CountUp, els, winheight, offset);

    // On scroll, loop through each element.
    document.addEventListener('scroll', function() {
        doScrollVisible(CountUp, els, winheight, offset);

    }, Modernizr.passiveeventlisteners ? {passive: true} : false);
}

/**
 * Add a 'visible' class to all elements matching the given selector if they are visible in the viewport.
 *
 * @param {CountUp} CountUp
 * @param {NodeList} els
 * @param {float} winheight
 * @param {integer} offset
 *
 * @return {void}
 */
function doScrollVisible(CountUp, els, winheight, offset) {
    for (var i=0; i<els.length; i++) {
        // This element position relative to the viewport.
        var rect = els[i].getBoundingClientRect();
        // Element offset - how far from the top of the viewport is it?
        var eloffset = rect.top;
        // Height of the rendered element.
        var elheight = els[i].offsetHeight;

        // Element centre / window height * 100.
        // Centre of the element's position as a percentage of the screen.
        var pos = ((eloffset + (elheight / 2)) / winheight) * 100;
        els[i].style.backgroundPosition = '50% ' + pos + '%';

        // Add offsets to top/bottom of each element for the on-screen detection.
        // On screen X pixels into the element, and off screen X pixels before the end of the element.
        // The height needs to have double the offset, to take into consideration the offset added to the
        // top of the element.
        eloffset += offset;
        elheight -= (offset * 2);

        // If the top of the element has passed the bottom of the screen,
        // and the bottom of the element hasn't gone off the top of the screen.
        // Only do the CountUp once.
        if ((eloffset <= winheight) && ((eloffset + elheight) >= 0) && !els[i].classList.contains('visible')) {

            els[i].classList.add('visible');

            // Get all stat values inside this stat section.
            var vals = els[i].querySelectorAll('.wrapper .stats .stat .value-container');

            // Initiate the count on each stat value.
            vals.forEach(function(val) {
                var from = val.getAttribute('data-from');
                var to = val.getAttribute('data-to');

                if(parseFloat(to)) {
                    // Val, from, to, decimals, seconds.
                    var animate = new CountUp(val.querySelector('.value'), from, to, 0, 2);

                    if(!animate.errors) {
                        animate.start();
                    }
                }

            });
        }
    }
}

module.exports = function($, CountUp, Modernizr) {
    $(document).ready(function() {
        scrollVisible(CountUp, 'section.count-up', 200);
    });
}
