module.exports = function($) {
    $(document).ready(function() {
        // Search bar show/hide.
        $('.toggle-search').click(function(e) {
            e.preventDefault();

            var el = $('.search-form');

            el.toggleClass('toggled');

            if(el.hasClass('toggled')) {
                document.getElementById('s').focus();
            }
        });

        $('.hide-search').click(function(e) {
            e.preventDefault();

            $('.search-form').removeClass('toggled');
        });
    });
}
