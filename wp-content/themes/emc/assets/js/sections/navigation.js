/**
 * Decide whether the header should be sticky or not.
 */
handleStickyHeader = function($) {
    var headerheight = $('header').outerHeight();
    var navheight = $('header .main-nav').outerHeight();

    // Always sticky on mobile, sticky from 44px onwards on desktop.
    if($(window).width() >= 768 && $(window).scrollTop() >= (headerheight - navheight)) {
        $('header').css('padding-bottom', navheight + 'px');
        $('header .main-nav').addClass('sticky');
    } else {
        $('header').css('padding-bottom', '0px');
        $('header .main-nav').removeClass('sticky');
    }
}


module.exports = function($, Modernizr) {
    $(document).ready(function() {
        // Dropdown mobile navigation.
        $('.burger').click(function(e) {
            e.preventDefault();

            $('.menu.main-menu').toggleClass('toggled');
        });

        // Clicking a li element on mobile shows its dropdown.
        $('.menu-item-has-children').click(function(e) {
            e.stopPropagation();

            if($(window).width() < 992) {
                $(this).toggleClass('toggled');
            }
        });

        // Clicking a li>a element on mobile opens the link.
        $('.menu-item-has-children a').click(function(e) {
            e.stopPropagation();
        });


        handleStickyHeader($);
    })

    document.addEventListener('scroll', function() {
        handleStickyHeader($);

    }, Modernizr.passiveeventlisteners ? {passive: true} : false);
}
