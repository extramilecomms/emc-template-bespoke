<section class="accordion">
	<div class="wrapper">
        <div class="written">
            <?php echo $data['content']; ?>
        </div>
        <?php foreach($data['accordion'] as $acc): ?>
            <div class="item">
                <h3>
                    <span><?php echo $acc['pc_acc_acc_title']; ?></span>
                    <i class="fas fa-chevron-down">
                    </i><i class="fas fa-chevron-up"></i>
                </h3>
                <div class="written">
                    <?php echo $acc['pb_acc_acc_content']; ?>
                </div>
            </div>
        <?php endforeach; ?>
	</div>
</section>