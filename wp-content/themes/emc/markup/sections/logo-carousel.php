<section class="logo-carousel">
	<div class="wrapper">
		<div class="written">
            <?php echo $data['content']; ?>
        </div>
        <?php if(count($data['logos'])): ?>
            <div class="logos owl-carousel">
                <?php foreach($data['logos'] as $logo): ?>
                    <div class="logo">
                        <a href="<?php echo $logo['pb_lc_logo_url']; ?>" target="_blank">
                            <?php $img = $logo['pb_lc_logo_image']; ?>
                            <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
	</div>
</section>