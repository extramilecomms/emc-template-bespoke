<section class="count-up" style="background-image: url('<?php echo $data['bk']; ?>');">
    <div class="wrapper">
        <?php if(count($data['stats'])): ?>
            <div class="stats">
                <?php foreach($data['stats'] as $stat): ?>
                    <div class="stat">
                        <p
                            class="value-container"
                            data-from="<?php echo $stat['pb_cu_stat_countfrom']; ?>"
                            data-to="<?php echo $stat['pb_cu_stat_value']; ?>"
                            aria-label="<?php echo $stat['pb_cu_stat_prefix'] . $stat['pb_cu_stat_value'] . $stat['pb_cu_stat_suffix']; ?>"
                        >
                            <span class="prefix" aria-hidden="true"><?php echo $stat['pb_cu_stat_prefix']; ?></span>
                            <span class="value" aria-hidden="true"><?php echo $stat['pb_cu_stat_value']; ?></span>
                            <span class="suffix" aria-hidden="true"><?php echo $stat['pb_cu_stat_suffix']; ?></span>
                        </p>
                        <p class="label"><?php echo $stat['pb_cu_stat_label']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
	</div>
</section>
