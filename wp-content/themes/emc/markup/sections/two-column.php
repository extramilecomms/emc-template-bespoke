<section class="two-column js less
overflow-hidden"
>
	<div class="wrapper
    md:flex items-center justify-between"
    >

		<div class="written
        flex-1 transform -translate-x-full transition-transform transition-opacity duration-150 mb-6 md:mb-0 md:mr-12"
        >
            <?php echo $data['left']; ?>
		</div>

		<div class="written
        flex-1 transform translate-x-full transition-transform transition-opacity duration-150"
        >
            <?php echo $data['right']; ?>
		</div>

	</div>
</section>