<section class="banner banner-<?php echo $data['size']; echo ($data['bcrumbs'] == 'yes' ? ' banner-breadcrumbs' : ''); echo (count($data['slides']) >= 2 ? ' owl-carousel banner-loading' : ''); ?>">

    <?php $i=0; foreach($data['slides'] as $slide): ?>

        <div
            class="item"
            style="background-image: url('<?php echo $slide['pb_b_slide_background']; ?>');"
        >
            <div class="wrapper">
                <?php if($i == 0): ?>
                    <h1 class="headline"><?php echo $slide['pb_b_slide_headline']; ?></h1>
                <?php else: ?>
                    <p class="headline"><?php echo $slide['pb_b_slide_headline']; ?></p>
                <?php endif; ?>

                <p class="content"><?php echo $slide['pb_b_slide_content']; ?></p>

                <?php if($slide['pb_b_slide_buttons'] && count($slide['pb_b_slide_buttons'])): ?>
                    <div class="buttons">
                        <?php foreach($slide['pb_b_slide_buttons'] as $button): ?>
                            <a
                                href="<?php echo (filter_var($button['btn_u'], FILTER_VALIDATE_URL) ? $button['btn_u'] : get_site_url() . $button['btn_u']); ?>"
                                class="button button-<?php echo $button['btn_c'] ?>"
                            ><?php echo $button['btn_t'] ?></a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>

    <?php $i++; endforeach; ?>

</section>

<?php if($data['bcrumbs'] == 'yes'): ?>
    <section class="breadcrumb">
        <div class="wrapper">
            <div class="crumbs">
                <ul>
                    <?php if(is_front_page()): ?>
                        <li>Home</li>
                    <?php else: ?>
                        <li><a href="<?php echo get_home_url(); ?>">Home</a></li>
                    <?php endif; ?>

                    <?php if(get_post_ancestors($post->post_parent) != null): ?>
                        <?php $grandparent = get_post(get_post_ancestors($post->post_parent)[0]); ?>
                        <li class="separator"><i class="fas fa-chevron-right"></i></li>
                        <li>
                            <a href="<?php echo get_permalink($grandparent); ?>">
                                <?php echo get_the_title($grandparent); ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if($post->post_parent): ?>
                        <li class="separator"><i class="fas fa-chevron-right"></i></li>
                        <li>
                            <a href="<?php echo get_permalink($post->post_parent); ?>">
                                <?php echo get_the_title($post->post_parent); ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php $categories = get_the_category(); ?>
                    <?php if($post->post_type == 'post' && count($categories)): ?>
                        <li class="separator"><i class="fas fa-chevron-right"></i></li>
                        <li>
                            <a href="/newsroom">
                                <?php echo $categories[0]->name; ?>
                            </a>
                        </li>
                    <?php endif; ?>

                    <li class="separator"><i class="fas fa-chevron-right"></i></li>
                    <li class="current"><?php echo $post->post_title; ?></li>

                </ul>
            </div>

            <?php if($data['share'] == 'yes'): ?>
                <?php echo do_shortcode('[addtoany url="' . get_permalink() . '" title="' . get_the_title() . '"]'); ?>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>

<script>
    var bannerAnimation = '<?php echo $data['animation']; ?>';
</script>