<section class="page-content <?php echo ($data['sidebar'] == 'yes' ? 'page-content-sidebar' : ''); ?>">
    <div class="wrapper">
        <?php if($data['content']): ?>
            <div class="content">
                <?php foreach($data['content'] as $section): ?>

                    <?php if($section['acf_fc_layout'] == 'pb_pc_content_editor'): ?>
                        <div class="written">
                            <?php echo $section['pb_pc_content_editor_editor'] ?>
                        </div>
                    <?php elseif($section['acf_fc_layout'] == 'pb_pc_content_contact'): ?>
                        <div class="two-column no-animation">
                        	<div class="wrapper">
                        		<div class="tc-container tc-container-left written">
                                    <?php echo do_shortcode($section['pb_pc_content_contact_form']); ?>
                        		</div>
                        		<div class="tc-container tc-container-right written">

                                    <h2>Get in touch</h2>
                                    <div class="contact-box">
                                        <p>
                                            <span>Phone Number:</span>
                                            <br>
                                            <a href="tel:<?php echo get_field('wo_company_phone', 'option'); ?>"><?php echo get_field('wo_company_phone', 'option'); ?></a>
                                        </p>
                                        <p>
                                            <span>Email Address:</span>
                                            <br>
                                            <a href="mailto:<?php echo get_field('wo_company_email', 'option'); ?>"><?php echo get_field('wo_company_email', 'option'); ?></a>
                                        </p>
                                        <p>
                                            <span>Registered Address:</span>
                                            <br>
                                            <?php echo get_field('wo_company_address', 'option'); ?>
                                            <br>
                                            <a href="<?php echo get_field('wo_company_map_link', 'option'); ?>">View on map</a>
                                        </p>
                                        <p>
                                            <span>Company Registration Number:</span>
                                            <br>
                                            <?php echo get_field('wo_company_number', 'option'); ?>
                                        </p>
                                    </div>

                        		</div>
                        	</div>
                        </div>
                    <?php endif; ?>

                <?php endforeach; ?>
            </div>
        <?php endif; ?>
        <?php if($data['sidebar'] == 'yes'): ?>
            <div class="sidebar">
                <?php
                $posts = get_posts([
                    'numberposts' => -1,
                    'post_parent' => $post->post_parent,
                    'post_type'   => 'page'
                ]);
                ?>
                <?php if(count($posts)): ?>
                    <ul class="sidebar-menu">
                        <li class="sidebar-title"></li>
                        <?php foreach ($posts as $post): setup_postdata($post); ?>
                            <li><a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a></li>
                        <?php endforeach; ?>
                    </ul>

                    <?php wp_reset_postdata(); ?>

                <?php endif; ?>

                <?php if($data['cta']): ?>
                    <div class="sidebar-contact">
                        <?php echo $data['cta']; ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</section>