<section class="cards cards-<?php echo $data['type']; ?>">
    <div class="wrapper">
        <?php
        if($data['content']) {
            echo '<div class="written">' . $data['content'] . '</div>';
        }

        if($data['type'] == 'custom') {
            $cards = $data['cards'];

        } elseif($data['type'] == 'all-category') {
            $cards = get_posts([
                'category'    => implode (',', $data['category']),
                'post_type'   => 'post',
                'numberposts' => -1
            ]);

        } elseif($data['type'] == 'three-category') {
            $cards = get_posts([
                'category'    => implode (',', $data['category']),
                'post_type'   => 'post',
                'numberposts' => 3
            ]);

        } elseif($data['type'] == 'children') {
            $cards = get_posts([
                'numberposts' => -1,
                'post_parent' => $post->ID,
                'post_type'   => 'page'
            ]);
        }

        if(count($cards)) {
            if($data['type'] == 'custom') {
                ?>
                <div class="cards-wrapper">
                    <?php foreach($cards as $card): setup_postdata($card); ?>

                        <?php $url = (filter_var($card['pb_c_card_buttons'][0]['btn_u'], FILTER_VALIDATE_URL) ? $card['pb_c_card_buttons'][0]['btn_u'] : get_site_url() . $card['pb_c_card_buttons'][0]['btn_u']); ?>

                        <div
                            class="card card-<?php echo $card['pb_c_card_type']; ?> card-<?php echo $card['pb_c_card_size']; ?>"
                            style="<?php echo ($card['pb_c_card_type'] == 'noimg' && $card['pb_c_card_colour'] ? ' background-color:' . $card['pb_c_card_colour'] : ' background-color:#f3f5f6'); ?>"
                        >
                            <?php
                            if($card['pb_c_card_type'] == 'halfimg' && $card['pb_c_card_image']) {
                                echo '
                                    <a href="' . $url . '">
                                        <div class="halfimg" style="background-image: url(\'' . $card['pb_c_card_image']['url'] . '\')"></div>
                                    </a>
                                ';
                            }
                            if($card['pb_c_card_type'] == 'bkimg') {
                                echo '
                                    <div class="bkimg" style="background-image: url(\'' . $card['pb_c_card_image']['url'] . '\')"></div>
                                ';
                            }
                            ?>
                            <div class="card-inner">
                                <?php
                                if($card['pb_c_card_headline']) {
                                    echo '
                                        <a href="' . $url . '">
                                            <h3>' . $card['pb_c_card_headline'] . '</h3>
                                        </a>
                                    ';
                                }
                                if($card['pb_c_card_content']) {
                                    echo '<p class="content">' . $card['pb_c_card_content'] . '</p>';
                                }
                                if(count($card['pb_c_card_buttons'])): ?>
                                    <div class="buttons">
                                        <?php foreach($card['pb_c_card_buttons'] as $button): ?>
                                            <a
                                                href="<?php echo (filter_var($button['btn_u'], FILTER_VALIDATE_URL) ? $button['btn_u'] : get_site_url() . $button['btn_u']); ?>"
                                                class="button button-<?php echo $button['btn_c'] ?>"
                                            ><?php echo $button['btn_t'] ?></a>
                                        <?php endforeach; ?>
                                    </div>
                                <?php
                                endif;
                                ?>
                            </div>
                        </div>

                    <?php endforeach; wp_reset_postdata(); ?>
                </div>
                <?php
            } else {
                ?>
                <div class="cards-wrapper">
                    <?php foreach($cards as $post): setup_postdata($post); ?>

                        <?php
                        if(get_post_type() == 'page') {
                            $img = get_field('pb', $post->ID)[0]['pb_b_slides'][0]['pb_b_slide_background'];
                            $headline = get_the_title();
                            $content = get_field('pb', $post->ID)[0]['pb_b_slides'][0]['pb_b_slide_headline'];
                            $btn = 'Read More';
                        } else {
                            $img = get_the_post_thumbnail_url();
                            $headline = get_the_title();
                            $content = custom_excerpt(20);
                            $btn = 'Read More';
                        }
                        ?>

                        <div class="card <?php echo ($img ? 'card-halfimg' : 'card-noimg-light'); ?> card-normal" style="background-color: #f3f5f6;">
                            <?php
                            if($img) {
                                echo '
                                    <a href="' . get_permalink() . '">
                                        <div class="halfimg" style="background-image: url(\'' . $img . '\')"></div>
                                    </a>
                                ';
                            }
                            ?>
                            <div class="card-inner">
                                <?php
                                if($headline) {
                                    echo '
                                        <a href="' . get_permalink() . '">
                                            <h3>' . $headline . '</h3>
                                        </a>
                                    ';
                                }
                                if($content) {
                                    echo '<p class="content">' . $content . '</p>';
                                }
                                ?>
                                <div class="buttons">
                                    <a
                                        href="<?php echo get_permalink(); ?>"
                                        class="button button-primary"
                                    ><?php echo $btn; ?></a>
                                </div>
                            </div>
                        </div>

                    <?php endforeach; wp_reset_postdata(); ?>
                </div>
                <?php
            }
        }
        ?>
    </div>
</section>