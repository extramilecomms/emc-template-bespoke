module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [
      "./**/*.php",
      "./**/*.less"
  ],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
};