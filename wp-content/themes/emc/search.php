<?php
/* Search results template. */

get_header();

// Banner.
// Required data.
$data = [
    'size'      => 'small',
    'animation' => 'default',
    'bcrumbs'   => 'no',
    'slides'    => [
        [
            'pb_b_slide_background' => 'http://localhost/emc-template-1/wp-content/uploads/2018/08/pexels-photo-39811.jpeg',
            'pb_b_slide_headline'   => 'Search results for: ' . get_search_query(),
            'pb_b_slide_content'    => '',
            'pb_b_slide_buttons'    => []
        ]
    ]
];

// Include markup.
include __DIR__ . '/markup/sections/banner.php';
?>

<section class="cards cards-rows">
    <div class="wrapper">
        <div class="cards-wrapper">

            <?php if(have_posts()): ?>
    			<?php while(have_posts()): the_post(); ?>
                    <?php
                    $is_page = ($post->post_type == 'post' ? false : true);

                    if($is_page) {
                        $img = get_field('pb')[0]['pb_b_slides'][0]['pb_b_slide_background'];
                        $headline = get_the_title();
                        $btn = 'Read More';
                    } else {
                        $img = get_the_post_thumbnail_url();
                        $headline = get_the_title();
                        $btn = 'Read More';
                    }
                    ?>
                    <div class="card card-halfimg card-normal">

                        <?php
                        if($img) {
                            echo '
                                <a href="' . get_permalink() . '">
                                    <div class="halfimg" style="background-image: url(\'' . $img . '\')"></div>
                                </a>
                            ';
                        }
                        ?>
                        <div class="card-inner">
                            <?php
                            if($headline) {
                                echo '
                                    <a href="' . get_permalink() . '">
                                        <h3>' . $headline . '</h3>
                                    </a>
                                ';
                            }
                            ?>
                            <div class="buttons">
                                <a
                                    href="<?php echo get_permalink(); ?>"
                                    class="button button-primary"
                                ><?php echo $btn; ?></a>
                            </div>
                        </div>
                    </div>
    			<?php endwhile; ?>
    		<?php endif; ?>

    		<?php wp_reset_postdata(); ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>
