<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?php wp_title('|', true, 'right'); ?></title>

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
        <header>
            <div class="wrapper">
                <div class="top">
                    <div class="social">
                        <?php if(have_rows('wo_socialmedia', 'option')): ?>
                            <?php while(have_rows('wo_socialmedia', 'option')): the_row(); ?>
                                <a href="<?php echo get_sub_field('wo_socialmedia_url'); ?>" aria-label="Visit our <?php echo get_sub_field('wo_socialmedia_website')['label'] ?> account">
                                    <i class="<?php echo get_sub_field('wo_socialmedia_website')['value']; ?>"></i>
                                </a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="misc">
                        <a href="tel:<?php echo get_field('wo_company_phone', 'option'); ?>" class="phone"><i class="fas fa-phone"></i> <span><?php echo get_field('wo_company_phone', 'option'); ?></span></a>
                        <a href="#" class="toggle-search"><i class="fas fa-search"></i></a>
                        <a href="#" class="burger" aria-label="Toggle the main menu"><i class="fas fa-bars"></i></a>
                    </div>
                </div>
                <div class="main-nav">
                    <div class="wrapper">
                        <form class="search-form" id="search-form" method="get" action="<?php echo get_site_url(); ?>">
                            <a href="#" onclick="document.getElementById('search-form').submit()" class="submit-search" title="Search our website"><i class="fas fa-search" aria-label="Search icon"></i></a>
                            <input type="text" name="s" id="s" placeholder="Enter a search term..." required>
                            <a href="#" class="hide-search" title="Close the search bar"><i class="fas fa-times" aria-label="Close icon"></i></a>
                        </form>
                        <?php wp_nav_menu(['theme_location' => 'main-menu', 'container' => 'nav', 'container_class' => 'menu main-menu']); ?>
                    </div>
                </div>
                <div class="site-logo">
                    <a href="<?php echo get_site_url(); ?>">
                        <?php $img = get_field('wo_company_logo', 'option'); ?>
                        <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
                    </a>
                </div>
            </div>
        </header>