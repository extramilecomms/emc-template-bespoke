        <footer>
            <div class="top">
                <div class="wrapper">
                    <?php if(get_field('wo_footer_newsletter', 'option') == 'visible'): ?>
                        <div class="newsletter">
                            <p class="title">Subscribe to our newsletter</p>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/email.png" aria-hidden="true">
                            <?php echo do_shortcode('[ninja_form id=1]'); ?>
                        </div>
                    <?php endif; ?>
                    <?php if(have_rows('wo_socialmedia', 'option')): ?>
                        <div class="social">
                            <p class="title">Follow Us</p>
                            <?php while(have_rows('wo_socialmedia', 'option')): the_row(); ?>
                                <a href="<?php echo get_sub_field('wo_socialmedia_url'); ?>" aria-label="Visit our <?php echo get_sub_field('wo_socialmedia_website')['label'] ?> account">
                                    <i class="<?php echo get_sub_field('wo_socialmedia_website')['value']; ?>"></i>
                                </a>
                            <?php endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="middle">
                <div class="wrapper">
                    <div class="site-logo">
                        <a href="<?php echo get_site_url(); ?>">
                            <?php $img = get_field('wo_company_logo_alt', 'option'); ?>
                            <img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>">
                        </a>

                        <p class="copyright">&copy; Copyright <?php echo date('Y'); ?> <?php echo get_field('wo_company_name', 'option'); ?></p>
                    </div>
                    <nav class="footer-menu">
                        <?php wp_nav_menu(['theme_location' => 'footer-menu', 'container' => '']); ?>
                    </nav>
                </div>
            </div>
            <div class="bottom">
                <div class="wrapper">
                    <p class="ra">Registered Address: <?php echo get_field('wo_company_address', 'option'); ?></p>
                    <?php if(is_front_page()): ?>
                        <p class="emc"><a href="https://extramilecommunications.com" target="_blank">Website by <span>ExtraMile</span></a></p>
                    <?php else: ?>
                        <p class="emc">Website by ExtraMile</p>
                    <?php endif; ?>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
