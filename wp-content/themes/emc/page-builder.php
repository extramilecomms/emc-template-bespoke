<?php

/* Template Name: Page Builder */


get_header();

if(have_rows('pb')) {
    while (have_rows('pb')) {
        the_row();

        // Banner.
		if(get_row_layout() == 'pb_b') {
            $data = [
                'size'      => get_sub_field('pb_b_size'),
                'animation' => get_sub_field('pb_b_animation'),
                'bcrumbs'   => get_sub_field('pb_b_breadcrumb'),
                'slides'    => get_sub_field('pb_b_slides')
            ];
            include __DIR__ . '/markup/sections/banner.php';

        // Two Column layout.
        } elseif(get_row_layout() == 'pb_tc') {
            $data = [
                'left'  => get_sub_field('pb_tc_left'),
                'right' => get_sub_field('pb_tc_right')
            ];
            include __DIR__ . '/markup/sections/two-column.php';

        // Cards layout.
        } elseif(get_row_layout() == 'pb_c') {
            $data = [
                'content'  => get_sub_field('pb_c_content'),
                'type'     => get_sub_field('pb_c_type'),
                'cards'    => get_sub_field('pb_c_cards'),
                'category' => get_sub_field('pb_c_category')
            ];
            include __DIR__ . '/markup/sections/cards.php';

        // Count Up layout.
        } elseif(get_row_layout() == 'pb_cu') {
            $data = [
                'bk'    => get_sub_field('pb_cu_background'),
                'stats' => get_sub_field('pb_cu_stats')
            ];
            include __DIR__ . '/markup/sections/count-up.php';

        // Logo Carousel layout.
        } elseif(get_row_layout() == 'pb_lc') {
            $data = [
                'content' => get_sub_field('pb_lc_content'),
                'logos'   => get_sub_field('pb_lc_logos')
            ];
            include __DIR__ . '/markup/sections/logo-carousel.php';

        // Page Content layout.
        } elseif(get_row_layout() == 'pb_pc') {
            $data = [
                'sidebar' => get_sub_field('pb_pc_show_sidebar'),
                'cta'     => get_sub_field('pb_pc_sidebar_cta'),
                'content' => get_sub_field('pb_pc_content')
            ];
            include __DIR__ . '/markup/sections/page-content.php';

        // Accordion.
        } elseif(get_row_layout() == 'pb_acc') {
            $data = [
                'content' => get_sub_field('pc_acc_content'),
                'accordion' => get_sub_field('pb_acc_accordion')
            ];
            include __DIR__ . '/markup/sections/accordion.php';
        }
    }
}


get_footer();

?>