<?php
/**
 * Plugin Name: EMC Customer Area
 * Description: Let selected customers log in and access protected pages.
 * Version: 0.0.1
 */




 /**
  * Activate the EMC Customer Area plugin.
  *
  * @return null
  */
 function emcca_activiation() {
 }
 register_activation_hook(__FILE__, 'emcca_activiation');


 /**
  * Deactivate the EMC Customer Area plugin.
  *
  * @return null
  */
 function emcca_deactiviation() {
 }
 register_deactivation_hook(__FILE__, 'emcca_deactiviation');


 /**
  * Uninstall the EMC Customer Area plugin.
  *
  * @return null
  */
 function emcca_uninstall() {
 }
 register_uninstall_hook(__FILE__, 'emcca_uninstall');




/**
 * Add a page for the customer area to the WP admin Settings dropdown.
 *
 * @return void
 */
function emcca_settings_menu() {
	add_options_page('EMC Customer Area', 'EMC Customer Area', 'manage_options', 'emc-customer-area', 'emcca_settings_content');
}
add_action('admin_menu', 'emcca_settings_menu');


/**
 * Return (or output) HTML for the plugin settings.
 *
 * @return string
 */
function emcca_settings_content() {
	if(!current_user_can('manage_options')) {
		wp_die('You do not have sufficient permissions to access this page.');
	}

    ?>
    <div class="wrap">
        <h1>ExtraMile Communications Customer Area Settings</h1>
    </div>
    <?php
}




/**
 * Add a panel for the customer area options to every post and page.
 *
 * @return void
 */
function emcca_add_meta_boxes() {
    add_meta_box(
        'emcca-options',
        'Customer Area Options',
        'emcca_meta_html',
        ['post', 'page'],
        'side',
        'default'
    );
}
add_action('add_meta_boxes', 'emcca_add_meta_boxes');


/**
 * Return (or output) HTML to add form fields to the page.
 *
 * @return string
 */
function emcca_meta_html() {
    global $post;
    global $wp_roles;

    wp_nonce_field('emcca_nonce', 'emcca_nonce');


    $protectedpage = get_post_meta($post->ID, 'emcca_protected_page', true);
    ?>
    <p class="post-attributes-label-wrapper page-template-label-wrapper">
        <label class="post-attributes-label" for="emcca-protected-page">Customer area page</label>
	</p>
    <select name="emcca-protected-page" id="emcca-protected-page">
        <option value="0"<?php echo ($protectedpage ? '' : ' selected'); ?>>No</option>
		<option value="1"<?php echo ($protectedpage ? ' selected' : ''); ?>>Yes</option>
    </select>

    <?php
    // Remove administrator from the list of roles. Admins can always view all pages.
    $roles = array_filter($wp_roles->roles, function($role, $slug) {
        return ($slug != 'administrator');
    }, ARRAY_FILTER_USE_BOTH);

    $userroleaccess = get_post_meta($post->ID, 'emcca_user_role_access', true);
    ?>
    <p class="post-attributes-label-wrapper page-template-label-wrapper">
        <label class="post-attributes-label" for="emcca-protected-page">Which users can access this page?</label>
	</p>
    <select name="emcca-user-role-access[]" id="emcca-user-role-access" multiple>
        <?php foreach($roles as $slug => $role): ?>
            <option value="<?php echo $slug; ?>"<?php echo (in_array($slug, $userroleaccess) ? ' selected' : ''); ?>><?php echo $role['name']; ?></option>
        <?php endforeach; ?>
    </select>
    <?php
}


/**
 * When the post is saved, check the data and save it.
 *
 * @param int $post_id
 *
 * @return void
 */
function emcca_save_meta_box_data($post_id) {

    // Check if the nonce is set.
    if(!isset($_POST['emcca_nonce'])) {
        return;
    }

    // Verify that the nonce is valid.
    if(!wp_verify_nonce($_POST['emcca_nonce'], 'emcca_nonce')) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Check the user's permissions.
    if(isset($_POST['post_type']) && $_POST['post_type'] == 'page') {
        if(!current_user_can('edit_page', $post_id)) {
            return;
        }
    } else {
        if(!current_user_can('edit_post', $post_id)) {
            return;
        }
    }

    // Make sure that it is set.
    if(!isset($_POST['emcca-protected-page'])) {
        return;
    }


    // Update the meta fields in the database.
    update_post_meta($post_id, 'emcca_protected_page', !!($_POST['emcca-protected-page']));
    update_post_meta($post_id, 'emcca_user_role_access', $_POST['emcca-user-role-access']);
}
add_action('save_post', 'emcca_save_meta_box_data');




/**
 * If required, protect the page and check if user can access the page.
 *
 * @return void
 */
function emcca_protect_page() {
    // Check if this page should be protected.
    // If the page shouldn't be protected, return and skip everything else below.
    if(!get_post_meta(get_the_ID(), 'emcca_protected_page', true)) {
        return;
    }


    // Check if the user is logged in.
    // If not, redirect to the login page (or homepage if a login page isn't specified).
    if(!is_user_logged_in()) {
        do_action('emcca_vistor_not_logged_in');
    }


    // Check if the user meets the required role to access this page.
    // If not, show a 403 Forbidden error.

    // Administrators can access every page.
    $roles = get_post_meta(get_the_ID(), 'emcca_user_role_access', true);
    $roles[] = 'administrator';

    $user = wp_get_current_user();

    if(!array_intersect($roles, $user->roles)) {
        do_action('emcca_invalid_user_permissions', $roles, $user);
    }


    // Do any other actions/checks that other devs might require for a site.
    do_action('emcca_additional_access_checks');
}
add_action('template_redirect', 'emcca_protect_page');


/**
 * The default action executed if a visitor isn't logged in.
 * Redirect them to the login page if it's specified, otherwise redirect to the homepage.
 *
 * @return void
 */
function emcca_redirect_to_login() {
    $loginPage = get_option('emcca_login_page');
    wp_redirect(get_site_url() . ($loginPage ? $loginPage : ''), 302);
    exit;
}
add_action('emcca_vistor_not_logged_in', 'emcca_redirect_to_login', 999, 2);


/**
 * The default action executed if a user doesn't have a required role to access a page.
 *
 * @param array $allowedroles
 * @param object $user
 *
 * @return void
 */
function emcca_throw_403_error($allowedroles, $user) {
    header('HTTP/1.0 403 Forbidden');
    echo '<h1>403 Forbidden</h1>';
    die('You are not allowed to access this page.');
}
add_action('emcca_invalid_user_permissions', 'emcca_throw_403_error', 999, 2);